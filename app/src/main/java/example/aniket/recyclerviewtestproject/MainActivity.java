package example.aniket.recyclerviewtestproject;

import android.database.Cursor;
import android.database.MatrixCursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class MainActivity extends AppCompatActivity
{
    private static final int NUMBER_OF_ROWS = 200;
    private RecyclerView mRecyclerView1;
    private RecyclerView mRecyclerView2;

    private TestAdapter mTestAdapter1;
    private Test2Adapter mTestAdapter2;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Cursor cursor = getDummyCursor();

        mRecyclerView1 = (RecyclerView) findViewById(R.id.recyclerView1);
        mRecyclerView2 = (RecyclerView) findViewById(R.id.recyclerView2);
        mRecyclerView1.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView2.setLayoutManager(new LinearLayoutManager(this));

        mTestAdapter1 = new TestAdapter(this, cursor);
        mRecyclerView1.setAdapter(mTestAdapter1);

        mTestAdapter2 = new Test2Adapter(cursor);
        mRecyclerView2.setAdapter(mTestAdapter2);

    }

    private Cursor getDummyCursor()
    {
        MatrixCursor matrixCursor = new MatrixCursor(new String[]{"name"});
        for (int i = 0; i < NUMBER_OF_ROWS;i++){
            String name = " ROW " + i;
            matrixCursor.addRow(new String[]{name});
        }
        return matrixCursor;
    }
}

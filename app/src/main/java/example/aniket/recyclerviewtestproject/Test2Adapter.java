package example.aniket.recyclerviewtestproject;

import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Vinod on 1/6/2017.
 */

public class Test2Adapter extends RecyclerView.Adapter
{
    Cursor mCursor;

    public Test2Adapter(Cursor cursor){
        mCursor = cursor;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder passedViewHolder, int position)
    {
        mCursor.moveToPosition(position);
        Log.d(TestAdapter.class.getSimpleName(), "onBindViewholder Called: " + position);
        ViewHolder viewHolder = (ViewHolder) passedViewHolder;
        viewHolder.name.setText(mCursor.getString(mCursor.getColumnIndex("name")));
    }

    @Override
    public int getItemCount()
    {
        return mCursor.getCount();
    }



    protected class ViewHolder extends RecyclerView.ViewHolder
    {
        public View parent;
        public TextView name;

        public ViewHolder(View itemView)
        {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.text);
            parent = itemView;
        }

    }
}

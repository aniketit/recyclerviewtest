package example.aniket.recyclerviewtestproject;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Vinod on 1/6/2017.
 */

public class TestAdapter extends CursorRecyclerViewAdapter
{
    public TestAdapter(Context context, Cursor cursor)
    {
        super(context, cursor);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder passedViewHolder, Cursor cursor)
    {
        Log.d(TestAdapter.class.getSimpleName(), "onBindViewholder Called: " + cursor.getPosition());
        ViewHolder viewHolder = (ViewHolder) passedViewHolder;
        viewHolder.name.setText(cursor.getString(cursor.getColumnIndex("name")));
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemView);
        return viewHolder;
    }

    protected class ViewHolder extends RecyclerView.ViewHolder
    {
        public View parent;
        public TextView name;

        public ViewHolder(View itemView)
        {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.text);
            parent = itemView;
        }

    }


}
